class DFSgen{
	constructor(graphMatrix){
		// matrika this.graphMatrix
		this.graphMatrix = graphMatrix;
		// dodatne ovire ki bodo usmerjale DFS v pravo smer
		// format [{x:10, y:3}, {x:3, y:2}]
		this.ovire = [];
		
		// visited matrika, ki pove katera vozlisca so ze bila obiskana
		this.visitedMatrix = null;

		// najde zacetno pozicijo
		this.start_position;

		// vozlisca kjer je pot do cilja
		this.pathList = [];

		this.allVisitedNodes = [];

		this.fitnesScore = 0;
	}


	calculateALLDistance(){
		var razdaljaALL = 0;
		for(var i = 0; i < this.allVisitedNodes.length; i++){
			razdaljaALL += this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x];
		}
		return razdaljaALL;
	}

	// zacne izvajanje dfs algoritma
	start(){
		this.visitedMatrix = this.createVisitedMatrix();
		this.dfs(this.start_position);

		// izracuna fitnes
		this.fitnesScore = this.fitnes();
	}


	// podam tabelo ovir
	dodajOvire(ov){
		for(var i in ov){
			this.ovire.push(ov[i]);
			this.graphMatrix[ov[i].y][ov[i].x] = -4;
		}
	}

	// izracuna kaksno iskalno pot je porabil do resitve
	fitnes(){
		var cenaVsega = 0;
		// v primeru da ni nasel poti vrne max velikost vseh poti
		if(this.pathList.length == 0){
			for(var y in this.graphMatrix){
				for(var x in this.graphMatrix[0]){
					if(this.graphMatrix[y][x] > 0) cenaVsega += this.graphMatrix[y][x];
				}
			}
		}
		else{
			for(var i in this.allVisitedNodes){
				cenaVsega += this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x];
			}
		}
		

		return cenaVsega;
	}

	// vrne nakljucno stevilo v podanem obmocju
	getRandomInt(min, max) {
	    min = Math.ceil(min);
	    max = Math.floor(max);
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	// nakljucno postavljene ovire
	// num predstavlja stevilo ovir
	randomObstacles(num){
		var postavitve = 0;

		while(postavitve < num){
			var pos = {x:this.getRandomInt(0, this.graphMatrix[0].length - 1),
					   y:this.getRandomInt(0, this.graphMatrix.length - 1)};
			// preveri ali je mesto veljavno
			if(this.graphMatrix[pos.y][pos.x] > 0){
				this.ovire.push(pos);
				this.graphMatrix[pos.y][pos.x] = -4;
				postavitve ++;
			}
		}
		
	}

	// vrne pozicijo mesta ki je prosto, na katerega lahko postavim oviro
	availableRandomPlace(){
		while(true){
			var pos = {x:this.getRandomInt(0, this.graphMatrix[0].length - 1),
					   y:this.getRandomInt(0, this.graphMatrix.length - 1)};
			// preveri ali je mesto veljavno
			if(this.graphMatrix[pos.y][pos.x] > 0){
				return pos;
			}
		}
	}

	createVisitedMatrix(){
		var tempMatrix = [];

		// sedaj vse -1 spremenim v true ostalo pa false
		for(var y = 0; y < this.graphMatrix.length; y++){
			var line = [];
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x] == -1 || this.graphMatrix[y][x] == -4) line.push(true);
				else if(this.graphMatrix[y][x] == -2){
					line.push(false);
					this.start_position = {x:x, y:y};
				}
				else line.push(false);
			}
			tempMatrix.push(line);
		}

		return tempMatrix;
	}

	 // glede na podano ogljisce vrne soseda ki ima najvecjo prioriteto
	 // Najvecjo prioriteto ima sosed ki ima najmanjso ceno
	getUnvisitedNeighbour(node){
		var neighbours = [];
		// imam lahko najvec 4 sosede, pregledati moram vse strani

		// top
		if(this.visitedMatrix[node.y - 1][node.x] != undefined && 
		   this.visitedMatrix[node.y - 1][node.x] == false){
			neighbours.push({x:node.x, y:node.y - 1});
		}

		// bottom
		if(this.visitedMatrix[node.y + 1][node.x] != undefined && 
		   this.visitedMatrix[node.y + 1][node.x] == false){
			neighbours.push({x:node.x, y:node.y + 1});
		}

		// left
		if(this.visitedMatrix[node.y][node.x - 1] != undefined && 
		   this.visitedMatrix[node.y][node.x - 1] == false){
			neighbours.push({x:node.x - 1, y:node.y});
		}

		// right
		if(this.visitedMatrix[node.y][node.x + 1] != undefined && 
		   this.visitedMatrix[node.y][node.x + 1] == false){
			neighbours.push({x:node.x + 1, y:node.y});
		}

		var neighbour = null;
		// grem skozi vse sosede in vrnem tistega ki ima najmanjso ceno
		for(var i = 0; i < neighbours.length; i++){
			if(neighbour == null) neighbour = neighbours[i];
			if(this.graphMatrix[neighbour.y][neighbour.x] > 
			   this.graphMatrix[neighbours[i].y][neighbours[i].x]){
			   	neighbour = neighbours[i];
			   }	
		}

		return neighbour;
	}


	dfs(node){
		// preneha iskati, ko najde cilj
		if(this.graphMatrix[node.y][node.x] == -3){
			return 0;
		} 
		// shrani si vsa obiskana vozlisca
		if(this.graphMatrix[node.y][node.x] != -2)this.allVisitedNodes.push(node);
		
		// oznaci trenutno vozlisce kot visited
		this.visitedMatrix[node.y][node.x] = true;
		// pridobi naslednje vozlisce v katerega se spusti
		var nextNode = this.getUnvisitedNeighbour(node);
		while(nextNode != null){
			var v = this.dfs(nextNode);
			// preveri ali je v tej veji nasel cilj
			if(v != null){
				if(this.graphMatrix[node.y][node.x] != -2){
					this.pathList.push({x:node.x, y:node.y});
					return v + this.graphMatrix[node.y][node.x];
				}
				else{
					return v;
				}
				
			} 
			nextNode = this.getUnvisitedNeighbour(node);
		}
		// ce pride do tu pomeni da ni nasel v tem delu koncno vozlisce, zato vrne null
		return null;
	}

	calculateDistance(){
		var razdalja = 0;
		for(var i = 0; i < this.pathList.length; i++){
			razdalja += this.graphMatrix[this.pathList[i].y][this.pathList[i].x];
		}
		return razdalja;
	}


	drawPath(){
		ctx.clearRect(0,0,canvas.width, canvas.height);
		ctx.beginPath();
		
		// izracuna width in height enege celice
		var w = canvas.width / this.graphMatrix[0].length;
		var h = canvas.height / this.graphMatrix.length;
		
		// izracuna velikost fonta
		var fontSize =parseInt(h * 0.85) 
		ctx.font =  fontSize + "px arial";

		for(var y = 0; y < this.graphMatrix.length; y++){
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x] >= 0){
					ctx.fillStyle = "#000000";
					ctx.rect(x * w, y * h,w,h);
					ctx.fillText(this.graphMatrix[y][x] + "",
								 x * w + (w - fontSize * 0.7) / 2, 
								 y * h + h - (h - fontSize * 0.8) / 2);
				}
				else if(this.graphMatrix[y][x] == -1){
					ctx.fillStyle = "#4c474f";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x] == -2){
					ctx.fillStyle = "#00FF00";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x] == -3){
					ctx.fillStyle = "#FF0000";
					ctx.fillRect(x * w, y * h,w,h);
				}

				else if(this.graphMatrix[y][x] == -4){
					ctx.fillStyle = "#000000";
					ctx.fillRect(x * w, y * h,w,h);
				}

				
			}
			
		}
		// oznaci vsa vozlisca po katerih se je algoritem spustil
		for(var i in this.allVisitedNodes){
			ctx.fillStyle = "#f4f142";
			ctx.fillRect(this.allVisitedNodes[i].x * w, this.allVisitedNodes[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x] + "",
								 this.allVisitedNodes[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.allVisitedNodes[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		// oznaci vozlisca po katerih je veljavna pot
		for(var i in this.pathList){
			ctx.fillStyle = "#4cbaff";
			ctx.fillRect(this.pathList[i].x * w, this.pathList[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.pathList[i].y][this.pathList[i].x] + "",
								 this.pathList[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.pathList[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		ctx.closePath();
		ctx.stroke();
	}
}