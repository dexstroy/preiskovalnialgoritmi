class BFS{
	constructor(graphMatrix){
		// matrika this.graphMatrix
		this.graphMatrix = graphMatrix;

		// visited matrika, ki pove katera vozlisca so ze bila obiskana
		this.visitedMatrix = this.createVisitedMatrix();

		// najde zacetno pozicijo
		this.start_position;
		
		// vozlisca kjer je pot do cilja
		this.pathList = [];
		this.allVisitedNodes = [];
		this.razdalja = 0;

		this.razdaljaALL = 0;

		// za vsako vozljisce mi pove iz katerega je prisel
		this.fromMatrix = [];
		for(var y = 0; y < this.graphMatrix.length; y++){
			var vrstica = [];
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				vrstica.push(null);
			}
			this.fromMatrix.push(vrstica);
		}


		//this.pot = this.bfs(this.start_position);
		this.bfs(this.start_position);

		this.calculateALLDistance();

	}

	calculateALLDistance(){
		for(var i = 0; i < this.allVisitedNodes.length; i++){
			this.razdaljaALL += this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x];
		}
		console.log("DFS razdalja ALL: " + this.razdaljaALL);
	}


	createVisitedMatrix(){
		var tempMatrix = [];

		// sedaj vse -1 spremenim v true ostalo pa false
		for(var y = 0; y < this.graphMatrix.length; y++){
			var line = [];
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x] == -1) line.push(true);
				else if(this.graphMatrix[y][x] == -2){
					line.push(false);
					this.start_position = {x:x, y:y};
				}
				else line.push(false);
			}
			tempMatrix.push(line);
		}

		return tempMatrix;
	}

	 // glede na podano ogljisce vrne soseda ki ima najvecjo prioriteto
	 // Najvecjo prioriteto ima sosed ki ima najmanjso ceno
	getUnvisitedNeighbour(node){
		var neighbours = [];
		// imam lahko najvec 4 sosede, pregledati moram vse strani

		// top
		if(this.visitedMatrix[node.y - 1][node.x] != undefined && 
		   this.visitedMatrix[node.y - 1][node.x] == false){
			neighbours.push({x:node.x, y:node.y - 1});
		}

		// bottom
		if(this.visitedMatrix[node.y + 1][node.x] != undefined && 
		   this.visitedMatrix[node.y + 1][node.x] == false){
			neighbours.push({x:node.x, y:node.y + 1});
		}

		// left
		if(this.visitedMatrix[node.y][node.x - 1] != undefined && 
		   this.visitedMatrix[node.y][node.x - 1] == false){
			neighbours.push({x:node.x - 1, y:node.y});
		}

		// right
		if(this.visitedMatrix[node.y][node.x + 1] != undefined && 
		   this.visitedMatrix[node.y][node.x + 1] == false){
			neighbours.push({x:node.x + 1, y:node.y});
		}

		var neighbour = null;
		// grem skozi vse sosede in vrnem tistega ki ima najmanjso ceno
		for(var i = 0; i < neighbours.length; i++){
			if(neighbour == null) neighbour = neighbours[i];
			if(this.graphMatrix[neighbour.y][neighbour.x] > 
			   this.graphMatrix[neighbours[i].y][neighbours[i].x]){
			   	neighbour = neighbours[i];
			   }	
		}

		return neighbour;
	}


	bfs(node){
		// naredim vrsto
		var q = new Queue();
		// dodam notri zacetni node
		q.enqueue(node);

		// dokler ne najde je true
		var iskanje = true;
		// ponavlja, dokler ima v vrsti elemente
		while(q.size() > 0 && iskanje){
			var n = q.dequeue();
			this.visitedMatrix[n.y][n.x] = true;
			if(this.graphMatrix[n.y][n.x] != -2) this.allVisitedNodes.push(n);
			// dobi soseda od tega noda
			var sosed = this.getUnvisitedNeighbour(n);
			while(!(sosed == null)){
				// pove iz katerega dela je prisel
				this.fromMatrix[sosed.y][sosed.x] = n;

				// preverim ali sem nasel iskani node in zapisem pot
				if(this.graphMatrix[sosed.y][sosed.x] == -3){
					//console.log("Nasel -3");
					var vertex = this.fromMatrix[sosed.y][sosed.x];
					while(!(vertex == null)){
						if(this.graphMatrix[vertex.y][vertex.x] != -2) {
							this.pathList.push(vertex);
							this.razdalja += this.graphMatrix[vertex.y][vertex.x];
						}
						vertex = this.fromMatrix[vertex.y][vertex.x];	
					}
					console.log("BFS razdalja: " + this.razdalja);
					// preklicem gnezdeni zanki ker sem nasel pot
					iskanje = false;
					break;
				}
				// oznacim ga kot visited
				this.visitedMatrix[sosed.y][sosed.x] = true;
				// povem kdo je njegov predhodnik
				this.fromMatrix[sosed.y][sosed.x] = n;
				// vstavim ga v vrsto
				q.enqueue(sosed);
				//console.log(this.graphMatrix[sosed.y][sosed.x]);
				sosed = this.getUnvisitedNeighbour(n);
			}
		}
	}


	drawPath(){
		ctx.clearRect(0,0,canvas.width, canvas.height);
		ctx.beginPath();
		
		// izracuna width in height enege celice
		var w = canvas.width / this.graphMatrix[0].length;
		var h = canvas.height / this.graphMatrix.length;
		
		// izracuna velikost fonta
		var fontSize =parseInt(h * 0.85) 
		ctx.font =  fontSize + "px arial";

		for(var y = 0; y < this.graphMatrix.length; y++){
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x] >= 0){
					ctx.fillStyle = "#000000";
					ctx.rect(x * w, y * h,w,h);
					ctx.fillText(this.graphMatrix[y][x] + "",
								 x * w + (w - fontSize * 0.7) / 2, 
								 y * h + h - (h - fontSize * 0.8) / 2);
				}
				else if(this.graphMatrix[y][x] == -1){
					ctx.fillStyle = "#4c474f";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x] == -2){
					ctx.fillStyle = "#00FF00";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x] == -3){
					ctx.fillStyle = "#FF0000";
					ctx.fillRect(x * w, y * h,w,h);
				}

				
			}
			
		}

		// oznaci vsa vozlisca po katerih se je algoritem spustil
		for(var i in this.allVisitedNodes){
			ctx.fillStyle = "#f4f142";
			ctx.fillRect(this.allVisitedNodes[i].x * w, this.allVisitedNodes[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x] + "",
								 this.allVisitedNodes[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.allVisitedNodes[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		for(var i in this.pathList){
			ctx.fillStyle = "#4cbaff";
			ctx.fillRect(this.pathList[i].x * w, this.pathList[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.pathList[i].y][this.pathList[i].x] + "",
								 this.pathList[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.pathList[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		ctx.closePath();
		ctx.stroke();
	}
	

}