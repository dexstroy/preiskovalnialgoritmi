class Queue{
	
	constructor(){
		this.list = [];
	}


	// doda element v vrsto
	enqueue(element){
		this.list.push(element);
	}

	// vrne element iz vrste
	dequeue(){
		// preveri ali lahko vrne kak elemet
		if(this.list.length == 0) return null;
		return this.list.shift();
	}

	// vrne stevilo elementov v vrsti
	size(){
		return this.list.length;
	}

}