class Stack{
	constructor(){
		this.list = [];
	}

	isEmpty(){
		return this.list.length == 0;
	}

	// doda element v stack
	push(element){
		this.list.push(element);
	}

	// vrne zgornji element iz stacka
	pop(){
		if(this.list.length == 0) return null;
		return this.list.pop();
	}

	// vrne vrhni element iz sklada, vendar ga ne popa iz sklada
	top(){
		if(this.list.length == 0) return null;
		return this.list[this.list.length - 1];
	}
}