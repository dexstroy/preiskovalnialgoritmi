// spremenjljivke za canvas in izris
var canvas;
var ctx;

// labirint
var labMatrix = [];

function readFile(path){
	var request = new XMLHttpRequest();
	request.open("GET", path);
	request.onreadystatechange = function () {
		if (request.readyState == 4) {
			loadModel(request.responseText);
		}
	}
	request.send();
}




var d = null;
var b = null;

var a = null;


var g = null;

// ko dobi podatke klice to fukcijo kjer napolni bufferje
function loadModel(data){
	labMatrix = [];
	var lines = data.split("\r\n");

	
	for(var i in lines){
		var line = lines[i].split(",");
		
		if(line.length > 1){
			for(var j in line){
				line[j] = parseInt(line[j]);
			}
			labMatrix.push(line);
		}
}

	
	b = new BFS(labMatrix);
	a = new Astar(labMatrix);
	d = new DFS(labMatrix);
	potTableUpdate();
	resetColor();
	startDFS();

	

	g = new Gen(labMatrix);
}

// pozene se ko se nalozi body
function start(){
	canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");

	selectChange();
}


function startDFS(){
	if(d != null ){
		d.drawPath();
		resetColor();
		document.getElementById("dfsLine").bgColor="#4cbaff";
		document.getElementById("labName").innerText = labList[labList.selectedIndex].text +
												 " DFS: " + d.razdalja;

	} 
}

function startBFS(){
	if(b != null ){
		b.drawPath();
		resetColor();
		document.getElementById("bfsLine").bgColor="#4cbaff";
		document.getElementById("labName").innerText = labList[labList.selectedIndex].text +
												 " BFS: " + b.razdalja;
	} 
}

function startASTAR(){
	if(a != null ){
		a.drawPath();
		resetColor();
		document.getElementById("aLine").bgColor="#4cbaff";
		document.getElementById("labName").innerText = labList[labList.selectedIndex].text +
												 " A*: " + a.razdalja;
	} 
}

function startGEN(){
	if(!(g.bestPerson == null)) g.bestPerson.drawPath();
	resetColor();
	document.getElementById("genDiv").style.display = "block";
	document.getElementById("gLine").bgColor="#4cbaff";
	document.getElementById("labName").innerText = labList[labList.selectedIndex].text +
												 " Genetski";
}

function simulateGen(){
	g.restart().drawPath();
	document.getElementById("gPath").innerText = g.bestPerson.calculateDistance();
	document.getElementById("ALLgPath").innerText = g.bestPerson.calculateALLDistance();
}

// ko izberem labirint se prozi ta event
function selectChange(){
	var labList = document.getElementById("labList");
	
	
	// prebere izbrano datoteko
	readFile("./data/labyrinth_" + 
		(labList.selectedIndex + 1) +
		".txt");
}


// vse vrstice obarva na osnovno barvo
function resetColor(){
	document.getElementById("dfsLine").bgColor="#FFFFFF";
	document.getElementById("bfsLine").bgColor="#FFFFFF";
	document.getElementById("aLine").bgColor="#FFFFFF";
	document.getElementById("gLine").bgColor="#FFFFFF";
	
	document.getElementById("genDiv").style.display = "none";


}

// posodobi vrednosti poti v tabeli
function potTableUpdate(){
	document.getElementById("dfsPath").innerText = d.razdalja;
	document.getElementById("bfsPath").innerText = b.razdalja;
	document.getElementById("aPath").innerText = a.razdalja;

	document.getElementById("ALLdfsPath").innerText = d.razdaljaALL;
	document.getElementById("ALLbfsPath").innerText = b.razdaljaALL;
	document.getElementById("ALLaPath").innerText = a.razdaljaALL;

	document.getElementById("gPath").innerText = "";
	document.getElementById("ALLgPath").innerText = "";
}