/*
Genetski algoritem katerega cilj je postaviti dodatne ovire,
ki bodo DFS algoritem usmerile v pravo smer in tako zmanjsal stevilo
potrebnik korakov do cilja.

Skozi potek evolucije bo shranil najboljso resitev
*/
class Gen{
	constructor(graphMatrix){
		this.graphMatrix = graphMatrix;

		// parametri ki vplivajo na potek evolucije
		// stevilo vir v labirintu
		this.obstacleNumber = 4;

		// lista v kateri bodo osebki
		this.population = [];

		// predstavja indekse v tabeli population
		// bolj kot je uspesen osebek, vec indeksov ima
		this.ruleta = [];

		// velikost populacije
		this.populationSize = 30;

		// Kaksna je verjetnost da posamezen blok zamenja pozicijo
		this.mutation_rate = 0.1;

		// stevilo ponovitev evolucije
		this.iterations = 50;


		// tu bo shranjen najboljsi osebek
		this.bestPerson = null;

		document.getElementById("genStOvir").value = this.obstacleNumber;
		document.getElementById("genVelPop").value = this.populationSize;
		document.getElementById("genMut").value = this.mutation_rate;
		document.getElementById("genStIt").value = this.iterations;


	}

	// vrne nakljucno stevilo v podanem obmocju
	getRandomInt(min, max) {
	    min = Math.ceil(min);
	    max = Math.floor(max);
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	// kreira populacijo in jo shrani v tabelo
	createPopulation(){
		this.population = new Array(this.populationSize)
		for(var i = 0; i < this.population.length; i++){
			// kreira osebek
			this.population[i] = new DFSgen(JSON.parse(JSON.stringify(this.graphMatrix)));
			this.population[i].randomObstacles(this.obstacleNumber);
			
		}
	}

	// prebere nove atribute iz podatkovnega polja
	restart(){
		this.obstacleNumber =  document.getElementById("genStOvir").valueAsNumber;
		this.populationSize =  document.getElementById("genVelPop").valueAsNumber;
		this.mutation_rate  =  document.getElementById("genMut").valueAsNumber;
		this.iterations  =  document.getElementById("genStIt").valueAsNumber;
		console.log(this.obstacleNumber);
		console.log(this.populationSize);
		console.log(this.mutation_rate);
		return this.getBestPerson();
	}

	/*
	Tu se bo odvijala evolucija glede na predpisana pravila, ter najdla najboljsi osebek
	*/
	getBestPerson(){
		this.bestPerson = null;
		// kreira populacijo
		this.createPopulation();
		// izvaja genetski algoritem
		for(var i = 0; i < this.iterations; i++){
			this.fitnes();
			this.selekcija();
			this.reprodukcija();
		}

		return this.bestPerson;
	}


	fitnes(){
		// grem skozi vso populacijo in poganjam DFS nad ovirami
		for(var i in this.population){
			this.population[i].start();
			// shranim si najboljsega osebka
			if(this.bestPerson == null || this.population[i].fitnes() < this.bestPerson.fitnes()){
				this.bestPerson = this.population[i];
			}
		}
	}
	//glede na dosezen fitnes score po vrsti uredi osebke od najslabsega do najboljsega
	selekcija(){
		this.population.sort(this.compare);
		this.ruleta = [];
		// napolnim ruleto s indeksi
		for(var i in this.population){
			for(var j = 0;  j <= i; j++){
				this.ruleta.push(i);
			}
		}
	}

	// glede na ruleto indeksov nakljucno izbere 2 osebka in naredi otroka
	// to ponavlja toliko casa, dokler nimamo nove populacije
	reprodukcija(){
		var tempPopulation = new Array(this.populationSize);
		for(var i = 0; i < tempPopulation.length; i++){
			// nakljucno izbere prvega in drugega osebka iz rutele
			var os1 = this.population[this.ruleta[this.getRandomInt(0, this.ruleta.length - 1)]];
			var os2 = this.population[this.ruleta[this.getRandomInt(0, this.ruleta.length - 1)]];

			// kreiram nov osebek
			tempPopulation[i] = new DFSgen(JSON.parse(JSON.stringify(this.graphMatrix)));
			
			// dodelim mu polovico genov prvega osebka
			tempPopulation[i].dodajOvire(os1.ovire.slice(0, os1.ovire.length/2));
			// sedaj grem skozi gene drugega osebka in jih dodajam toliko casa, dokler ni gen dolocene velikosti
			for(var h in os2.ovire){
				// preveri ali lahko prekine zanko
				if(tempPopulation[i].ovire.length == this.obstacleNumber) break;
				// preveri ali lahko naredi mutacijo enega gena
				if(Math.random() <= this.mutation_rate){
					tempPopulation[i].dodajOvire([tempPopulation[i].availableRandomPlace()])
				}
				else if(!this.inArray(tempPopulation[i].ovire, os2.ovire[h])){
					tempPopulation[i].dodajOvire([os2.ovire[h]]);
				}
			}
		}
		this.population = tempPopulation;
	}

	// preveri ali se pozicija nahaja v tabeli
	inArray(list, element){
		for(var j in list){
			if(list[j].x == element.x && list[j].y == element.y) return true;
		}
		return false;
	}

	compare(a,b) {
		if (a.fitnesScore < b.fitnesScore)
		return 1;
		if (a.fitnesScore > b.fitnesScore)
		return -1;
		return 0;
	}


}
