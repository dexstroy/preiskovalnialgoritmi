// Anode class predstavlja en node v grafu za Astar
class Anode{
	constructor(price, position){
		// cena trenutnega vozlisca
		this.price = price;

		// pozicija vozlisca v matriki
		this.position = position;

		// optimalna razdalja od zacetnega vozlisca
		this.fromStart = 0;
		// evklidska razdalja do cilja
		this.toFinish = 0;

		// pozicija prejsnjega noda
		this.fromNode = null;

		// true ko je node bil obiskan in popan iz prioritetne vrste
		// ce je zid ga da na true, da ga algoritem ignorira
		this.visited = price == -1;
	}

	// vrne utez trenutnega vozlisca 
	weight(){
		return this.fromStart + this.toFinish;
	}


}

// Prioritetna vrsta
class PriorityQueue{
	constructor(){
		this.list = [];
	}

	size(){
		return this.list.length;
	}

	isEmpty(){
		return this.list.length == 0;
	}

	// dodam v listo
	push(node){
		// ce se nima elementa ga vstavi na prvo mesto
		if(this.size() == 0){
			this.list.push(node);
			return;
		} 

		// sprehodi se skozi tabelo in vstavi element
		for(var i = 0; i < this.list.length; i++){
			if(node.weight() <= this.list[i].weight()){
				this.list.splice(i,0,node);
				return;
			}
			// preveri ali bo vstavljen na konec
			else if(i == this.list.length - 1){
				this.list.push(node);
				return;
			} 
		}
	}

	// vzamem z liste
	pop(){
		// preverim ali lahko sploh kaj vrnem
		if(this.isEmpty()) return null;
		return this.list.shift();
	}


}



class Astar{
	constructor(graphMatrix){
		// tu shrani zacetno pozicijo
		this.start_position;

		this.endPositions = [];

		// matrika this.graphMatrix
		this.graphMatrix = [];
		// sestavi novo matriko ki ima Anode objekte
		for(var y = 0; y < graphMatrix.length; y++){
			var line = [];
			for(var x = 0; x < graphMatrix[0].length; x++){
				line.push(new Anode(graphMatrix[y][x], {x:x, y:y}));
				// preveri ali je nasel zacetno pozicijo
				if(graphMatrix[y][x] == -2) this.start_position = {x:x, y:y};
				else if(graphMatrix[y][x] == -3) this.endPositions.push({x:x, y:y});
			}
			this.graphMatrix.push(line);
		}

		// vozlisca kjer je pot do cilja
		this.pathList = [];
		this.allVisitedNodes = [];
		// dolzina najdene poti
		this.razdalja = 0;

		this.razdaljaALL = 0;

		this.astar();
		//this.drawPath();

		this.calculateALLDistance();
	}


	calculateALLDistance(){
		for(var i = 0; i < this.allVisitedNodes.length; i++){
			this.razdaljaALL += this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x].price;
		}
		console.log("DFS razdalja ALL: " + this.razdaljaALL);
	}

	 // glede na podano ogljisce vrne sosede ki se niso bili obiskani
	getUnvisitedNeighbours(node){
		var neighbours = [];
		// imam lahko najvec 4 sosede, pregledati moram vse strani

		// top
		if(this.graphMatrix[node.y - 1][node.x] != undefined && 
		   this.graphMatrix[node.y - 1][node.x].visited == false){
			//neighbours.push({x:node.x, y:node.y - 1});
			neighbours.push(this.graphMatrix[node.y - 1][node.x]);
		}

		// bottom
		if(this.graphMatrix[node.y + 1][node.x] != undefined && 
		   this.graphMatrix[node.y + 1][node.x].visited == false){
			//neighbours.push({x:node.x, y:node.y + 1});
			neighbours.push(this.graphMatrix[node.y + 1][node.x]);
		}

		// left
		if(this.graphMatrix[node.y][node.x - 1] != undefined && 
		   this.graphMatrix[node.y][node.x - 1].visited == false){
			//neighbours.push({x:node.x - 1, y:node.y});
			neighbours.push(this.graphMatrix[node.y][node.x - 1]);
		}

		// right
		if(this.graphMatrix[node.y][node.x + 1] != undefined && 
		   this.graphMatrix[node.y][node.x + 1].visited == false){
			//neighbours.push({x:node.x + 1, y:node.y});
			neighbours.push(this.graphMatrix[node.y][node.x + 1]);
		}
		return neighbours;
	}


	// vrne evklidsko razdaljo med podanim nodom in najblizjim ciljem
	distance(n){
		var minDistance = 0;
		
		// izracuna distanco za prvi cilj
		var x1 = this.endPositions[0].x - n.position.x;
		var y1 = this.endPositions[0].y - n.position.y;
		minDistance = Math.sqrt(Math.pow(x1,2) + Math.pow(y1,2));

		for(var i = 0; i < this.endPositions.length; i++){
			x1 = this.endPositions[i].x - n.position.x;
			y1 = this.endPositions[i].y - n.position.y;
			var dist = Math.sqrt(Math.pow(x1,2) + Math.pow(y1,2));

			if(dist < minDistance) minDistance = dist;
		}

		return minDistance;
		/*
		var x1 = this.endPositions[0].x - n.position.x;
		var y1 = this.endPositions[0].y - n.position.y;

		return Math.sqrt(Math.pow(x1,2) + Math.pow(y1,2));
		*/
	}

	// izvede algoritem
	astar(){
		// prioritetna vrsta
		var q = new PriorityQueue();

		// notri dodam zacetni element
		q.push(this.graphMatrix[this.start_position.y][this.start_position.x]);

		// izvaja A*
		while(q.size() > 0){
			// popam node do katerega je do zdaj najbolj optimalna pot
			var node = q.pop();
			// dobim vse sosede v katere se lahko spustim
			var nodeList = this.getUnvisitedNeighbours(node.position);
			// grem skozi vsa vozlisca za trenutni node
			for(var i = 0; i < nodeList.length; i++){
				// preverim ali sem nasel cilj
				if(nodeList[i].price == -3){
					// shranim razdaljo
					this.razdalja = node.fromStart;
					console.log("A* razdalja: " + this.razdalja);
					// najde pot do tega noda 
					this.reversePath(node);
					return;
				}
				// izracunam pot do starta in preverim ali je bolj optimalna
				var potFromStart = node.fromStart + nodeList[i].price;
				if(potFromStart < nodeList[i].fromStart || nodeList[i].fromStart == 0){
					nodeList[i].fromStart = potFromStart;
					// popravim iz katerega noda je prisel
					nodeList[i].fromNode = node.position;
				}
				
				// izracunam koliko ima ta node do cilja
				nodeList[i].toFinish = this.distance(nodeList[i]);
				// ga dodam v vrsto
				q.push(nodeList[i]);
			}
			node.visited = true;
			if(node.price != -2)this.allVisitedNodes.push(node.position);
			// ko koncam vse operacije nad sosedi, shranim node nazaj v matriko
			this.graphMatrix[node.position.y][node.position.x] = node;
		}
	}
	
	// poisce pot za nazaj
	reversePath(node){
		while(!(node.fromNode == null)){
			this.pathList.push(node.position);
			node = this.graphMatrix[node.fromNode.y][node.fromNode.x];
			
		}
	}


	drawPath(){
		ctx.clearRect(0,0,canvas.width, canvas.height);
		ctx.beginPath();
		
		// izracuna width in height enege celice
		var w = canvas.width / this.graphMatrix[0].length;
		var h = canvas.height / this.graphMatrix.length;
		
		// izracuna velikost fonta
		var fontSize = parseInt(h * 0.85) 
		ctx.font =  fontSize + "px arial";

		for(var y = 0; y < this.graphMatrix.length; y++){
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x].price >= 0){
					ctx.fillStyle = "#000000";
					ctx.rect(x * w, y * h,w,h);
					ctx.fillText(this.graphMatrix[y][x].price + "",
								 x * w + (w - fontSize * 0.7) / 2, 
								 y * h + h - (h - fontSize * 0.8) / 2);
				}
				else if(this.graphMatrix[y][x].price == -1){
					ctx.fillStyle = "#4c474f";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x].price == -2){
					ctx.fillStyle = "#00FF00";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x].price == -3){
					ctx.fillStyle = "#FF0000";
					ctx.fillRect(x * w, y * h,w,h);
				}

				
			}
			
		}
		// oznaci vsa vozlisca po katerih se je algoritem spustil
		for(var i in this.allVisitedNodes){
			ctx.fillStyle = "#f4f142";
			ctx.fillRect(this.allVisitedNodes[i].x * w, this.allVisitedNodes[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x].price + "",
								 this.allVisitedNodes[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.allVisitedNodes[i].y * h + h - (h - fontSize * 0.8) / 2);
		}

		for(var i in this.pathList){
			ctx.fillStyle = "#4cbaff";
			ctx.fillRect(this.pathList[i].x * w, this.pathList[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.pathList[i].y][this.pathList[i].x].price + "",
								 this.pathList[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.pathList[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		ctx.closePath();
		ctx.stroke();
	}
	

}