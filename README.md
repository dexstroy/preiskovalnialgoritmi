# Game RapidMiner

## About
An example of finding the shortest path in a labyrinth using DFS, BFS, A*, and a genetic algorithm.

The genes in the genetic algorithm are defined by obstacles placed in the labyrinth. The fitness function returns the distance using the DFS algorithm run on the labyrinth with obstacles.

![Local Image](images/example.png)
