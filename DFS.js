class DFS{
	constructor(graphMatrix){
		// matrika this.graphMatrix
		this.graphMatrix = graphMatrix;

		// visited matrika, ki pove katera vozlisca so ze bila obiskana
		this.visitedMatrix = this.createVisitedMatrix();

		// najde zacetno pozicijo
		this.start_position;
		this.razdalja = 0;

		this.razdaljaALL = 0;

		// vozlisca kjer je pot do cilja
		this.pathList = [];

		this.allVisitedNodes = [];

		this.dfs(this.start_position);

		this.calculateDistance();
		this.calculateALLDistance();
		

		this.drawPath();
	}


	createVisitedMatrix(){
		var tempMatrix = [];

		// sedaj vse -1 spremenim v true ostalo pa false
		for(var y = 0; y < this.graphMatrix.length; y++){
			var line = [];
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x] == -1) line.push(true);
				else if(this.graphMatrix[y][x] == -2){
					line.push(false);
					this.start_position = {x:x, y:y};
				}
				else line.push(false);
			}
			tempMatrix.push(line);
		}

		return tempMatrix;
	}

	 // glede na podano ogljisce vrne soseda ki ima najvecjo prioriteto
	 // Najvecjo prioriteto ima sosed ki ima najmanjso ceno
	getUnvisitedNeighbour(node){
		var neighbours = [];
		// imam lahko najvec 4 sosede, pregledati moram vse strani

		// top
		if(this.visitedMatrix[node.y - 1][node.x] != undefined && 
		   this.visitedMatrix[node.y - 1][node.x] == false){
			neighbours.push({x:node.x, y:node.y - 1});
		}

		// bottom
		if(this.visitedMatrix[node.y + 1][node.x] != undefined && 
		   this.visitedMatrix[node.y + 1][node.x] == false){
			neighbours.push({x:node.x, y:node.y + 1});
		}

		// left
		if(this.visitedMatrix[node.y][node.x - 1] != undefined && 
		   this.visitedMatrix[node.y][node.x - 1] == false){
			neighbours.push({x:node.x - 1, y:node.y});
		}

		// right
		if(this.visitedMatrix[node.y][node.x + 1] != undefined && 
		   this.visitedMatrix[node.y][node.x + 1] == false){
			neighbours.push({x:node.x + 1, y:node.y});
		}

		var neighbour = null;
		// grem skozi vse sosede in vrnem tistega ki ima najmanjso ceno
		for(var i = 0; i < neighbours.length; i++){
			if(neighbour == null) neighbour = neighbours[i];
			if(this.graphMatrix[neighbour.y][neighbour.x] > 
			   this.graphMatrix[neighbours[i].y][neighbours[i].x]){
			   	neighbour = neighbours[i];
			   }	
		}

		return neighbour;
	}


	dfs(node){
		// preneha iskati, ko najde cilj
		if(this.graphMatrix[node.y][node.x] == -3){
			return 0;
		} 
		// shrani si vsa obiskana vozlisca
		if(this.graphMatrix[node.y][node.x] != -2)this.allVisitedNodes.push(node);
		
		// oznaci trenutno vozlisce kot visited
		this.visitedMatrix[node.y][node.x] = true;
		// pridobi naslednje vozlisce v katerega se spusti
		var nextNode = this.getUnvisitedNeighbour(node);
		while(nextNode != null){
			var v = this.dfs(nextNode);
			// preveri ali je v tej veji nasel cilj
			if(v != null){
				if(this.graphMatrix[node.y][node.x] != -2){
					this.pathList.push({x:node.x, y:node.y});
					return v + this.graphMatrix[node.y][node.x];
				}
				else{
					return v;
				}
				
			} 
			nextNode = this.getUnvisitedNeighbour(node);
		}
		// ce pride do tu pomeni da ni nasel v tem delu koncno vozlisce, zato vrne null
		return null;
	}

	calculateDistance(){
		for(var i = 0; i < this.pathList.length; i++){
			this.razdalja += this.graphMatrix[this.pathList[i].y][this.pathList[i].x];
		}
		console.log("DFS razdalja: " + this.razdalja);
	}

	calculateALLDistance(){
		for(var i = 0; i < this.allVisitedNodes.length; i++){
			this.razdaljaALL += this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x];
		}
		console.log("DFS razdalja ALL: " + this.razdaljaALL);
	}


	drawPath(){
		ctx.clearRect(0,0,canvas.width, canvas.height);
		ctx.beginPath();
		
		// izracuna width in height enege celice
		var w = canvas.width / this.graphMatrix[0].length;
		var h = canvas.height / this.graphMatrix.length;
		
		// izracuna velikost fonta
		var fontSize =parseInt(h * 0.85) 
		ctx.font =  fontSize + "px arial";

		for(var y = 0; y < this.graphMatrix.length; y++){
			for(var x = 0; x < this.graphMatrix[0].length; x++){
				if(this.graphMatrix[y][x] >= 0){
					ctx.fillStyle = "#000000";
					ctx.rect(x * w, y * h,w,h);
					ctx.fillText(this.graphMatrix[y][x] + "",
								 x * w + (w - fontSize * 0.7) / 2, 
								 y * h + h - (h - fontSize * 0.8) / 2);
				}
				else if(this.graphMatrix[y][x] == -1){
					ctx.fillStyle = "#4c474f";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x] == -2){
					ctx.fillStyle = "#00FF00";
					ctx.fillRect(x * w, y * h,w,h);
				}
				else if(this.graphMatrix[y][x] == -3){
					ctx.fillStyle = "#FF0000";
					ctx.fillRect(x * w, y * h,w,h);
				}

				
			}
			
		}
		// oznaci vsa vozlisca po katerih se je algoritem spustil
		for(var i in this.allVisitedNodes){
			ctx.fillStyle = "#f4f142";
			ctx.fillRect(this.allVisitedNodes[i].x * w, this.allVisitedNodes[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.allVisitedNodes[i].y][this.allVisitedNodes[i].x] + "",
								 this.allVisitedNodes[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.allVisitedNodes[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		// oznaci vozlisca po katerih je veljavna pot
		for(var i in this.pathList){
			ctx.fillStyle = "#4cbaff";
			ctx.fillRect(this.pathList[i].x * w, this.pathList[i].y * h,w,h);

			ctx.fillStyle = "#000000";
			ctx.fillText(this.graphMatrix[this.pathList[i].y][this.pathList[i].x] + "",
								 this.pathList[i].x * w + (w - fontSize * 0.7) / 2, 
								 this.pathList[i].y * h + h - (h - fontSize * 0.8) / 2);
		}
		ctx.closePath();
		ctx.stroke();
	}
	

}